# Integrations

_Status: alpha_

_**For developers**_

Busy features a very simple plugin architecture for integrations. It's still a work in progress and might change with any release.

Integrations are designed for external systems that need to sync data (in either direction or both) with the Busy "database".

Integrations must be preloaded into the venv from which Busy is run, typically within the `pipx` installation. In the future, we might provide a mechanism for loading them.

Two commands support integrations. Both tage the integration name as their first argument.

- `get` - to get a piece of data
- `sync` - to "sync" data


When an integration-related command is run, Busy will attempt to import a module called `busy_` plus the integration name. The module must be preloaded. General expectation is that the package name would start with `busy-` plus the same integration name. For example, to sync with Asana, an integration might come in a package called `busy-asana` containing a module called `busy_asana`.

The interface for integrations is still under development and will be documented when it becomes more stable.

The general expectation is that the `sync` method will perform some read and write activity in either of Busy or some external system, and raise an exception if the operation files. Additionally, it might return a value, for example if the integration is purely designed to generate output in a specific format.

The `sync` method doesn't generate a status - that's up to the `sync` command.

When developing an integration, include `busy` itself in `pyproject.toml`, ideally with a specific version or narrow range, to avoid the possibility of dependency conflicts.

Under current temporary guidelines, installing an integration might look something like this (example from MacsOS):

```bash
.local/pipx/venvs/busy/bin/python -m pip install busy-asana
```

Items support the following readable properties, all parsed from the markup

- **base** (str)
- **tags** - (set)
- **url** - (str)
- **repeat** (str)
- **elapsed** - (int)
- **data_value** - The only one that's a method; takes the key as input
- **markup** - Everything as entered, plus elapsed time as timing data

Note: Integrations first introduced in Busy 7.4.1 but considered unstable until at least 8.0.

## Ready to get Busy?

[Contribute!](contribute.md)