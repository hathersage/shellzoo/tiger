from unittest import TestCase
from unittest.mock import Mock, patch

from busy import BusyApp
from busy.command.add_command import AddCommand
from busy.model.collection.done_collection import DoneCollection
from busy.model.collection.plan_collection import PlanCollection
from busy.model.collection.todo_collection import TodoCollection
from busy.model.item import Item
from busy.test_case import BusyTestCase

# Simple example of how to test command class behaviour directly without having
# to actually parse arguments: Mock the app operations.


class TestCommandAdd(BusyTestCase):

    def test_add(self):
        a = Mock()
        a.storage.get_collection.return_value \
            = o = TodoCollection([Item('#a')])
        c = AddCommand(a, markup="jf")
        c.execute()
        self.assertEqual([str(x) for x in o], ['#a', 'jf'])
        self.assertTrue(o.changed)

    def test_add_omitted_markup(self):
        a = Mock()
        a.storage.get_collection.return_value = o = \
            TodoCollection([Item('#a')])
        a.ui.get_text.return_value = 'f'
        c = AddCommand(a)
        c.execute()
        self.assertEqual([str(x) for x in o], ['#a', 'f'])
        self.assertTrue(o.changed)

    def test_from_app(self):
        tc = TodoCollection()
        pc = PlanCollection()
        dc = DoneCollection()
        a = BusyApp()
        a.storage = self.mock_storage(tc, pc, dc)
        with \
                self.patchout() as o, self.patcherr() as e, \
                self.patch_ttyin(''):
            a.parse_run('add', 'this')
        self.assertIn('this', tc[-1].markup)
