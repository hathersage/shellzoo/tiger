import json
from time import sleep
from io import StringIO
from threading import Thread
from unittest.mock import patch

from busy import BusyApp
from busy.model.collection.todo_collection import TodoCollection
from busy.model.item import Item
from busy.test_case import BusyTestCase

MSG_INIT = json.dumps(
    {
        'jsonrpc': '2.0',
        'id': 1,
        'method': 'initialize',
        'params': {
            'protocolVersion': '2024-11-05',
            'capabilities': {
                'roots': {'listChanged': True},
                'sampling': {}
            },
            'clientInfo': {
                'name': 'TestClient',
                'version': '1.0.0'
            }
        }
    }
)

MSG_NOTIFY = '''{"jsonrpc": "2.0", "method": "notifications/initialized"}'''
MSG_PING = '''{"jsonrpc": "2.0", "id": 2,  "method": "ping"}'''
MSG_LIST = json.dumps(
    {
        'jsonrpc': '2.0',
        'id': 3,
        'method': 'tools/list'
    }
)
MSG_DO = json.dumps(
    {
        "jsonrpc": "2.0",
        "id": 4,
        "method": "tools/call",
        "params": {
            "name": "perform",
            "arguments": {
                "command": "view 1"
            }
        }
    }
)


class StringLineReader(StringIO):

    def __init__(self):
        super().__init__()
        self.cursor = 0

    def get_line(self) -> str:
        for _ in range(100):
            output = self.getvalue()
            if len(output) > self.cursor:
                new_output = output[self.cursor:].split('\n', maxsplit=1)
                if len(new_output) > 1:
                    response = new_output[0]
                    self.cursor += len(response) + 1
                    return response
            sleep(0.1)
        raise RuntimeError('StringLineReader response timeout')


class MCPCommandTest(BusyTestCase):

    def test_ping(self):

        with \
                patch('sys.stdin', StringIO()) as i, \
                patch('sys.stdout', StringLineReader()) as o, \
                patch('sys.stderr', StringIO()):
            server = Thread(target=BusyApp.start, args=('mcp',))
            server.daemon = True
            server.start()

            i.write(MSG_INIT + '\n')
            i.write(MSG_NOTIFY + '\n')
            i.write(MSG_PING + '\n')
            i.write(MSG_LIST + '\n')
            i.seek(0)

            r1 = o.get_line()
            r2 = o.get_line()
            r3 = o.get_line()

            i.write('\x04')  # EOF
            i.seek(0)
            server.join(timeout=1.0)
        j1 = json.loads(r1)
        self.assertEqual(j1['result']['serverInfo']['name'], "Busy")
        j2 = json.loads(r2)
        self.assertEqual(j2['result'], {})
        j3 = json.loads(r3)
        self.assertEqual(j3['result']['tools'][0]['name'], 'perform')

    def test_do(self):

        with \
                patch('sys.stdin', StringIO()) as i, \
                patch('sys.stdout', StringLineReader()) as o, \
                patch('sys.stderr', StringIO()):

            tc = TodoCollection([Item('a')])
            a = BusyApp()
            a.storage = self.mock_storage(tc, None)

            server = Thread(target=a.parse_run, args=('mcp',))
            server.daemon = True
            server.start()

            i.write(MSG_INIT + '\n')
            i.write(MSG_NOTIFY + '\n')
            i.write(MSG_DO + '\n')

            i.seek(0)

            r1 = o.get_line()
            r3 = o.get_line()

            i.write('\x04')  # EOF
            i.seek(0)
            server.join(timeout=1.0)
        j3 = json.loads(r3)
        rr = json.loads(j3['result']['content'][0]['text'])['result']
        self.assertEqual(rr, 'a')

    # def test_command(self):
    #     tc = TodoCollection()
    #     a = BusyApp()
    #     a.storage = self.mock_storage(tc, pc, dc)
    #     with \
    #             self.patchout() as o, self.patcherr() as e, \
    #             self.patch_ttyin(''):
    #         a.parse_run('add', 'this')
    #     self.assertIn('this', tc[-1].markup)
