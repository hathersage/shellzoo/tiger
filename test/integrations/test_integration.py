import importlib
from unittest import TestCase

from test import BusyMockIntegrationTestCase


class TestIntegration(BusyMockIntegrationTestCase):

    def test_import(self):
        """Confirm we can dynamically import an integration"""
        m = importlib.import_module('busy_mock_integration')
        a = m.Main().fake()
        self.assertEqual('j', a)
