from datetime import date, datetime
from io import StringIO
from pathlib import Path
import sys
from unittest.mock import Mock, patch

from busy import BusyApp
from busy import __main__

from busy.test_case import BusyTestCase
BusyApp.initialize()


class BusyMockIntegrationTestCase(BusyTestCase):
    """Inherit from this to get the mock integration. Note that the mock
    integration will remain imported for subsequent tests, but the import path
    will get reset."""

    @classmethod
    def setUpClass(cls):
        mock_integration_dir = Path.cwd() / 'mock-integration'
        cls.syspath = sys.path
        sys.path = cls.syspath + [str(mock_integration_dir)]

    @classmethod
    def tearDownClass(cls):
        sys.path = cls.syspath
