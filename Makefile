# Assume venv; for dev only. See also do/

.PHONY: init dependencies test style sandbox mock

PROJECT_NAME := $(shell basename "$(CURDIR)")

all: style test

# Set up a new venv
init:
	rm -rf .venv
	python3.11 -m venv .venv
	.venv/bin/pip install --upgrade pip poetry

# Install dependencies
dependencies:
	.venv/bin/poetry install --no-root

# Does this make sense?
mock:
	.venv/bin/pip install -e ./mock-integration

# Run unit tests and report coverage
test:
	.venv/bin/python -m coverage run --source=$(PROJECT_NAME) -m unittest
	.venv/bin/python -m coverage report -m --fail-under 96

# Check code style - use AutoPEP8 to clean up some
style:
	.venv/bin/autopep8 --experimental -ir .
	.venv/bin/pycodestyle $(PROJECT_NAME)
	.venv/bin/pycodestyle test

# Reset the sandbox for casual testing
sandbox:
	mkdir -p ./sandbox/working
	rm -rf ./sandbox/working/*
	cp -r ./sandbox/origin/* ./sandbox/working/